public class Cycle 
{
	/**
	 * define a "cycle" by number of wheels and it's weight
	 */
	private int numberOfWheels;
	private int weight;

	/**
	 * cycle's constructor
	 * @param numberOfWheels
	 * @param weight
	 */
	public Cycle(int numberOfWheels, int weight)
	{	
		this.numberOfWheels = numberOfWheels;
		this.weight = weight;
	}
	/**
	 * defualt values for cycle
	 */
	public Cycle()
	{
		this.numberOfWheels=100;
		this.weight=1000;
	}
	/**
	 * toString method
	 */
	public String toString()
	{	
		return	"Number of Wheels: "+numberOfWheels+"\n"+
				"Weight: "+weight;
	}
	
}
