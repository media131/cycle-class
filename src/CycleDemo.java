public class CycleDemo 
{
	public static void main(String[] args) 
	{
		/**
		 * this shows the default values
		 */
		Cycle defaultCycle = new Cycle();
		System.out.println(defaultCycle);
		/**
		 * this shows custom values
		 */
		Cycle customCycle = new Cycle(2,15);
		System.out.println(customCycle);
	}

}
